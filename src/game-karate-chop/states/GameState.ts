import AudioPlayer from '../classes/audio/AudioPlayer';
import Tree from '../classes/entities/Tree';
import Lumberjack from '../classes/entities/Lumberjack';
import LostLivesHeart from '../classes/ui/LostLivesHeart';
import PlayerInputs from '../classes/ui/PlayerInputs';
import UiFlash from '../classes/ui/UiFlash';
import { Game } from '../game';
import Listeners from '../Listeners';
import IKarateChopGameConfig, { ILevelData } from '../IKarateChopGameConfig';
import HelpPanel from '../classes/ui/HelpPanel';
import Lifebar from '../classes/ui/Lifebar';

export default class GameState extends Phaser.State {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private _audioPlayer: AudioPlayer;

  private _helpPanel: HelpPanel;

  private _tree: Tree;

  private _player: Lumberjack;

  private _levelProgressBar: Lifebar;

  private _playerInputs: PlayerInputs;

  private _backgroundGroup: Phaser.Group;

  private _entitiesGroup: Phaser.Group;

  private _uiGroup: Phaser.Group;

  private _incorrectOrientationImage: Phaser.Group;

  private _timer: Phaser.Timer;

  private _playerLevel: number;

  private _playerNumLogChops: number;

  private _playerCantMove: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Game) {
    super();
  }

  // ----------------------------------------------------------------------------------------------
  get audio(): AudioPlayer {
    return (this._audioPlayer);
  }

  // ----------------------------------------------------------------------------------------------
  create() {
    console.log('Game state [create] started');

    this.game.stage.backgroundColor = '#000000';

    setTimeout(() => {
      this.initOrientation();

      this.initTimer();
      this.initGroups();
      this.initAudioPlayer();
      this.initBackground();
      this.initUi();
      this.initTree();
      this.initPlayer();
      this.initBgm();

      if (this.shouldShowInstructions()) {
        this.presentInstructions();
      } else {
        this.startGame();
      }

      this.listenerCallback(Listeners.READY, this.game);
    }, 10);
  }

  // ----------------------------------------------------------------------------------------------
  get gameObject(): Game {
    return (<Game>this.game);
  }

  // ----------------------------------------------------------------------------------------------
  listenerCallback(key: string, ...args): void {
    const callback: Function = this.gameObject.listenerMapping[key];
    if (callback) {
      callback.call(this.game, ...args);
    }
  }

  // ----------------------------------------------------------------------------------------------
  shutdown(): void {
    this.audio.dispose();
    this._playerInputs.destroy();
    this.game = null;
  }

  // ----------------------------------------------------------------------------------------------
  get timer(): Phaser.Timer {
    return (this._timer);
  }

  // ----------------------------------------------------------------------------------------------
  get uiGroup(): Phaser.Group {
    return (this._uiGroup);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private advanceChopCount(): void {
    this.setPlayerProgress(this._playerNumLogChops + 1);
    if (this._playerNumLogChops >= this.currentLevel.numberOfChopsToWin) {
      this.winLevel();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private awardPointsForChop(): void {
    let points = this.currentLevel.pointsPerChop;
    if (points === undefined) {
      points = 1;
    }

    this.listenerCallback(Listeners.ADD_POINT, points);
  }

  // ----------------------------------------------------------------------------------------------
  private awardTimeForWinLevel(): void {
    const timeInSeconds = this.currentLevel.timeBonus;
    if (timeInSeconds > 0) {
      const IS_OFFSET = true;
      this.listenerCallback(Listeners.SET_SECONDS, timeInSeconds, IS_OFFSET);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private calcViewOffset(): number {
    const headerBarDiv = document.getElementById('headerBar');
    return (headerBarDiv ? headerBarDiv.clientHeight * this.game.scale.scaleFactor.y : 0);
  }

  // ----------------------------------------------------------------------------------------------
  get currentLevel(): ILevelData {
    return (this.karateChopConfig.levels[this._playerLevel]);
  }

  // ----------------------------------------------------------------------------------------------
  private enforceLevelPenalty(): void {
    if (this.currentLevel.levelPenalty === 'restart-level') {
      this.restartLevel();
    } else if (this.currentLevel.levelPenalty === 'reduce-level') {
      this.reduceLevel();
    } else if (this.currentLevel.levelPenalty === 'start-over') {
      this.startLevelsOver();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private enforcePointsPenalty(): number {
    let pointsPenalty = this.currentLevel.pointsPenalty;
    if (pointsPenalty === undefined) {
      pointsPenalty = 1;
    }

    this.listenerCallback(Listeners.ADD_POINT, -pointsPenalty);

    return (pointsPenalty)
  }

  // ----------------------------------------------------------------------------------------------
  private hideIncorrectOrientationMessage() {
    if (this._incorrectOrientationImage) {
      this._incorrectOrientationImage.destroy();
      this._incorrectOrientationImage = null;
    }

    this.game.paused = false;
  }

  // ----------------------------------------------------------------------------------------------
  private initAudioPlayer(): void {
    this._audioPlayer = new AudioPlayer(this.game);
  }

  // ----------------------------------------------------------------------------------------------
  private initBackground(): void {
    const viewOffset = this.calcViewOffset()
    const image = this.add.image(0, viewOffset, 'background', undefined, this._backgroundGroup);

    if (image.width < this.game.width) {
      image.width = this.game.width;
      image.scale.y = image.scale.x;
    }
  }

  // ----------------------------------------------------------------------------------------------
  private initBgm(): void {
    this.audio.play('bgm-main', undefined, undefined, undefined, true);
  }

  // ----------------------------------------------------------------------------------------------
  private initOrientation(): void {
    this.scale.onOrientationChange.add((scale: Phaser.ScaleManager,
      prevOrientation: string, wasIncorrect: boolean) => {
        const hasChanged = scale.screenOrientation !== prevOrientation;
        if (!hasChanged) {
          return;
        }

        const isIncorrect = scale.incorrectOrientation && !wasIncorrect;
        if (isIncorrect) {
          this.showIncorrectOrientationMessage();
        } else {
          this.hideIncorrectOrientationMessage();
        }
      });
  }

  // ----------------------------------------------------------------------------------------------
  private initGroup(): Phaser.Group {
    const group = this.game.add.group();
    return (group);
  }

  // ----------------------------------------------------------------------------------------------
  private initGroups(): void {
    this._backgroundGroup = this.initGroup();
    this._entitiesGroup = this.initGroup();
    this._uiGroup = this.initGroup();
  }

  // ----------------------------------------------------------------------------------------------
  private initLevelProgressBar(): void {
    this._levelProgressBar = new Lifebar(this.game, this.uiGroup);
    this._levelProgressBar.x = 153;
    this._levelProgressBar.y = 30 + this.calcViewOffset();
    this._levelProgressBar.visible = false;
  }

  // ----------------------------------------------------------------------------------------------
  private initPlayer(): void {
    this._player = new Lumberjack(this.game, this.calcViewOffset());
    this._player.isOnLeftSide = Math.random() >= 0.5;
    this._player.idle();
    this._entitiesGroup.add(this._player);
  }

  // ----------------------------------------------------------------------------------------------
  private initPlayerInputs(): void {
    this._playerInputs = new PlayerInputs(this.game, this._uiGroup, this.calcViewOffset());

    this._playerInputs.leftSignal.add(() => {
      this.playerAttack(true);
    });

    this._playerInputs.rightSignal.add(() => {
      this.playerAttack(false);
    });
  }

  // ----------------------------------------------------------------------------------------------
  // TODO: might need to re-use scaling and positioning
  /*
  private initTiles(): void {
    const logicalScale = this._tiles.width / 540;
    const width = this.game.scale.width * logicalScale;
    this._tiles.width = width;
    this._tiles.scale.y = this._tiles.scale.x;

    this._tiles.x = (this.game.scale.width - this._tiles.width) / 2;
    this._tiles.y = (this.game.scale.height - this._tiles.height) / 2;

    this.uiGroup.add(this._tiles);
  }
  */

  // ----------------------------------------------------------------------------------------------
  private initTimer(): void {
    this._timer = this.game.time.create(false);
    this._timer.start();
  }

  // ----------------------------------------------------------------------------------------------
  private initTree(): void {
    this._tree = new Tree(this.game, this.calcViewOffset(), this._entitiesGroup);
  }

  // ----------------------------------------------------------------------------------------------
  private initUi(): void {
    this.initPlayerInputs();
    this.initLevelProgressBar();
  }

  // ----------------------------------------------------------------------------------------------
  private get karateChopConfig(): IKarateChopGameConfig {
    return (this.gameObject.karateChopConfig);
  }

  // ----------------------------------------------------------------------------------------------
  private isPlayerSafe(): boolean {
    const isPlayerOnLeftSide = this._player.isOnLeftSide;
    const isPlayerOnRightSide = !isPlayerOnLeftSide;
    return (
      (isPlayerOnLeftSide !== this._tree.isNextBranchOnLeftSide())
      && (isPlayerOnRightSide !== this._tree.isNextBranchOnRightSide())
    );
  }

  // ----------------------------------------------------------------------------------------------
  private playerAttack(isOnLeftSide: boolean): void {
    this.removeHelpPanel();

    if (this._tree.areLogsDropping) {
      return;
    }

    if (this._playerCantMove) {
      return;
    }

    this._player.isOnLeftSide = isOnLeftSide;

    if (this.isPlayerSafe()) {
      this.playerAttackSuccess();
    } else {
      this.playerAttackFail();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private playerAttackFail(): void {
    this._player.defeat();

    this.startCantMoveTimer();

    this._tree.timberrrrr(this._player.isOnLeftSide);

    this.presentScreenTremor();
    this.presentScreenFlash();

    const pointsLost = this.enforcePointsPenalty();
    this.presentLostLivesHeart(-pointsLost);

    this.enforceLevelPenalty();

    this.audio.play('snd-crack');

    this.listenerCallback(Listeners.LIVES_LOST, this.game, 1);
  }

  // ----------------------------------------------------------------------------------------------
  private playerAttackSuccess(): void {
    this._player.attack();
    this._tree.timberrrrr(this._player.isOnLeftSide);

    this.awardPointsForChop();

    this.advanceChopCount();

    this.audio.play('snd-chop');
  }

  // ----------------------------------------------------------------------------------------------
  private presentInstructions(): void {
    this._helpPanel = new HelpPanel(this.game, this.uiGroup);
  }

  // ----------------------------------------------------------------------------------------------
  private presentLostLivesHeart(value: number): void {
    const heart = new LostLivesHeart(this.game, 0, 0, value);
    this.uiGroup.add(heart);
    heart.position.set(
      this._player.centerX,
      this._player.top - heart.height - 10,
    );
  }

  // ----------------------------------------------------------------------------------------------
  private presentScreenFlash(): void {
    if (this.karateChopConfig.isUiImmersive) {
      UiFlash.Create(this.game, 3, this.uiGroup);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private presentScreenTremor(): void {
    if (!this.karateChopConfig.isUiImmersive) {
      return;
    }

    const INTENSITY = 0.02;
    const DURATION = UiFlash.DEFAULT_FLASH_ON_DURATION + UiFlash.DEFAULT_FLASH_OFF_DURATION;
    this.camera.shake(INTENSITY, DURATION);
  }

  // ----------------------------------------------------------------------------------------------
  private reduceLevel(): void {
    const nextLevel = Math.max(0, this._playerLevel - 1);
    this.setLevel(nextLevel);
  }

  // ----------------------------------------------------------------------------------------------
  private removeHelpPanel(): void {
    if (!this._helpPanel) {
      return;
    }

    this._helpPanel.destroy();
    this._helpPanel = null;

    this.startGame();
  }

  // ----------------------------------------------------------------------------------------------
  private restartLevel(): void {
    this.setPlayerProgress(0);
  }

  // ----------------------------------------------------------------------------------------------
  private revivePlayer(): void {
    this._playerCantMove = false;
    this._player.revyve();
    this._player.isOnLeftSide = !this._player.isOnLeftSide;
  }

  // ----------------------------------------------------------------------------------------------
  private setLevel(level: number): void {
    const maxLevelIndex = this.karateChopConfig.levels.length - 1;
    this._playerLevel = Math.max(Math.min(maxLevelIndex, level), 0);

    this.setPlayerProgress(0);
    this.updateProgressBarColor();
    this._tree.createBranchChance = this.currentLevel.createBranchChance;

    this.listenerCallback(Listeners.SET_LEVEL, this._playerLevel);
  }

  // ----------------------------------------------------------------------------------------------
  private setPlayerProgress(playerNumLogChops: number): void {
    this._playerNumLogChops = playerNumLogChops;
    this._levelProgressBar.value = playerNumLogChops / this.currentLevel.numberOfChopsToWin;
  }

  // ----------------------------------------------------------------------------------------------
  private setStartLevel(): void {
    const level = this.karateChopConfig.startLevel || 0;
    this.setLevel(level);
  }

  // ----------------------------------------------------------------------------------------------
  private showIncorrectOrientationMessage() {
    if (this._incorrectOrientationImage) {
      return;
    }

    this.game.paused = true;
    this._incorrectOrientationImage = this.add.group();

    const graphics = this.add.graphics(0, 0, this._incorrectOrientationImage);
    graphics.beginFill(0, 1.0);
    graphics.drawRect(0, 0, this.game.width, this.game.height);
    graphics.endFill();

    const image = this.add.image(0, 0, 'incorrect-orientation-message', undefined,
      this._incorrectOrientationImage);
    image.anchor.set(0.5);
    image.x = this.game.width / 2;
    image.y = this.game.height / 2;

    image.width = this.game.width;
    image.scale.y = image.scale.x;

    if (image.height < this.game.height) {
      image.height = this.game.height
      image.scale.x = image.scale.y;
    }
  }

  // ----------------------------------------------------------------------------------------------
  private shouldShowInstructions(): boolean {
    const shouldShowHelp = this.karateChopConfig.shouldShowHelp;
    return (shouldShowHelp === true || shouldShowHelp === undefined);
  }

  // ----------------------------------------------------------------------------------------------
  private startCantMoveTimer(): void {
    const duration = this.gameObject.karateChopConfig.cantMoveDurationInSeconds;
    if (!duration) {
      this.revivePlayer();
      return;
    }

    this._playerCantMove = true;
    this.timer.add(duration * 1000, () => {
      this.revivePlayer();
    });
  }

  // ----------------------------------------------------------------------------------------------
  private startGame(): void {
    this._levelProgressBar.visible = true;

    this.setStartLevel();
    this.listenerCallback(Listeners.SET_SECONDS,
      this.gameObject.karateChopConfig.startTimeInSeconds);
  }

  // ----------------------------------------------------------------------------------------------
  private startLevelsOver(): void {
    this.setLevel(0);
  }

  // ----------------------------------------------------------------------------------------------
  private updateProgressBarColor(): void {
    let color = this.currentLevel.progressBarColor;
    if (color === undefined) {
      color = 0xff0000;
    }

    this._levelProgressBar.tint = color;
  }

  // ----------------------------------------------------------------------------------------------
  private winLevel(): void {
    const nextLevel = Math.min(this._playerLevel + 1, this.karateChopConfig.levels.length - 1);
    this.setLevel(nextLevel);
    this.awardTimeForWinLevel();
    this.audio.play('snd-win-level');
  }
}
