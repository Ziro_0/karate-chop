export default class PreloadState {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly BASE_PATH = 'assets/game-karate-chop/';

  private game: Phaser.Game;

  private preloadBar: Phaser.Image;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game) {
    this.game = game;
  }

  // ----------------------------------------------------------------------------------------------
  create(): void {
    this.game.state.start('MainMenuState');
  }

  // ----------------------------------------------------------------------------------------------
  preload(): void {
    this.preloadBar = this.game.add.image(
      this.game.world.centerX,
      this.game.world.centerY,
      'preload_bar',
    );

    this.preloadBar.anchor.set(0.5);

    this.game.load.setPreloadSprite(this.preloadBar);

    // load assets
    this.loadSpritesheets();
    this.loadImages();
    this.loadSounds();
    this.loadFonts();
    this.loadJsons();
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private loadFont(key: string): void {
    const path = `${PreloadState.BASE_PATH}`;
    this.game.load.bitmapFont(key, `${path}${key}.png`, `${path}${key}.xml`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadFontBfg(key: string): void {
    // Loads fonts created by Bitmap Font Generator. BFG appends a "_x"-like template onto the 
    // ends of its image files, which doesn't exactly match key (which doesn't use the template).
    const path = `${PreloadState.BASE_PATH}`;
    this.game.load.bitmapFont(key, `${path}${key}_0.png`, `${path}${key}.xml`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadFonts(): void {
    [
      'open-sans-bold',
    ].forEach((key) => {
      this.loadFont(key);
    });

    [].forEach((key) => {
      this.loadFontBfg(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadImage(key: string): void {
    this.game.load.image(key, `${PreloadState.BASE_PATH}${key}.png`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadImages(): void {
    [
      'background',
      'button-left-arrow',
      'button-right-arrow',
      'button-tap',
      'incorrect-orientation-message',
      'lifebar-fill',
      'lifebar-frame',
      'lost-lives-heart',
      'tree-left-branch',
      'tree-log',
      'tree-right-branch',
      'tree-stump',
    ].forEach((key) => {
      this.loadImage(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadJson(key: string): void {
    this.game.load.json(key, `${PreloadState.BASE_PATH}${key}.json`);
  }

  // ----------------------------------------------------------------------------------------------
  private loadJsons(): void {
    [
      'lumberjack1_7x2_animations',
    ].forEach((key) => {
      this.loadJson(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadSound(key: string): void {
    const baseUrl = `${PreloadState.BASE_PATH}${key}`;
    const mp3Url = `${baseUrl}.mp3`;
    const oggUrl = `${baseUrl}.ogg`;
    const urls = [
      mp3Url,
      oggUrl
    ];

    this.game.load.audio(key, urls);
  }

  // ----------------------------------------------------------------------------------------------
  private loadSounds(): void {
    [
      'bgm-main',
      'snd-chop',
      'snd-crack',
      'snd-win-level',
    ].forEach((key) => {
      this.loadSound(key);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private loadSpritesheet(key: string, frameWidth: number, frameHeight: number): void {
    const url = `${PreloadState.BASE_PATH}${key}.png`;
    this.game.load.spritesheet(key, url, frameWidth, frameHeight);
  }

  // ----------------------------------------------------------------------------------------------
  private loadSpritesheets(): void {
    [
      [ 'lumberjack1_7x2', 145, 152 ],
    ].forEach((data) => {
      this.loadSpritesheet(<string> data[0], <number> data[1], <number> data[2]);
    });
  }
}