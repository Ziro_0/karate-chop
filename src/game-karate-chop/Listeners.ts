enum Listeners {
  ADD_POINT = 'add-point',
  LIVES_LOST = 'lives-lost',
  READY = 'ready',
  SET_LEVEL = 'set-level',
  SET_SECONDS = 'set-seconds',
  TUTORIAL_OPEN = 'tutorial-open',
  TUTORIAL_CLOSE = 'tutorial-close',
}

export default Listeners;
