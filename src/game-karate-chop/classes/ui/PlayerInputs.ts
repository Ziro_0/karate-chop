export default class PlayerInputs extends Phaser.Signal {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static DEBUG_SHOW_ZONES = false;

  leftSignal: Phaser.Signal;

  rightSignal: Phaser.Signal;

  game: Phaser.Game

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, group: Phaser.Group, viewOffset = 0) {
    super();
    this.game = game;
    this.leftSignal = new Phaser.Signal();
    this.rightSignal = new Phaser.Signal();
    this.initZones(viewOffset, group);
    this.inputKeys();
  }

  // ----------------------------------------------------------------------------------------------
  destroy(): void {
    this.leftSignal.dispose();
    this.rightSignal.dispose();
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private initKey(keyCode: number, signal: Phaser.Signal): void {
    const key = this.game.input.keyboard.addKey(keyCode);
    key.onDown.add(() => {
      signal.dispatch(this);
    });
  }

  // ----------------------------------------------------------------------------------------------
  private inputKeys(): void {
    this.initKey(Phaser.KeyCode.LEFT, this.leftSignal);
    this.initKey(Phaser.KeyCode.RIGHT, this.rightSignal);
  }

  // ----------------------------------------------------------------------------------------------
  private initZone(x: number, viewOffset: number, group: Phaser.Group, signal: Phaser.Signal,
    debugColor?: number): void {
    const zone = this.game.add.graphics(x, 0 + viewOffset, group);
    if (PlayerInputs.DEBUG_SHOW_ZONES && debugColor !== undefined) {
      zone.beginFill(debugColor, 0.3);
    } else {
      zone.beginFill(0, 0);
    }

    zone.drawRect(0, 0, this.game.scale.width / 2, this.game.scale.height);
    zone.endFill();

    zone.inputEnabled = true;
    zone.events.onInputDown.add(() => {
      signal.dispatch(this);
    })
  }

  // ----------------------------------------------------------------------------------------------
  private initZones(viewOffset: number, group: Phaser.Group): void {
    // left zone
    let x = 0;
    this.initZone(x, viewOffset, group, this.leftSignal, 0xff0000);

    // right zone
    x = this.game.scale.width / 2;
    this.initZone(x, viewOffset, group, this.rightSignal, 0x0000ff);
  }
}
