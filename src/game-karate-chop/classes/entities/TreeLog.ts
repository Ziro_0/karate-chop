export default class TreeLog extends Phaser.Group {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static CHOP_DROP_DURATION_MS = 500;

  private static CHOP_SPIN_DURATION_MS = 200;

  private static LEFT_BRANCH_X = -84;

  private static RIGHT_BRANCH_X = 108;

  private static BRANCH_Y = 77;

  private _log: Phaser.Image;

  private _branch: Phaser.Image;

  private _hasLeftBranch: boolean | undefined;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, x: number, y: number, createBranchChance: number,
    group?: Phaser.Group) {
    super(game, group);
    this.x = x;
    this.y = y;
    this.initLog();
    this.initBranch(createBranchChance);
  }

  // ----------------------------------------------------------------------------------------------
  chop(isPlayerOnLeftSide: boolean): Promise<this> {
    this.bringThisToTop();
    this.chopBranch(isPlayerOnLeftSide);
    return (this.chopLog(isPlayerOnLeftSide));
  }

  // ----------------------------------------------------------------------------------------------
  get hasLeftBranch(): boolean {
    return (this._branch && this._hasLeftBranch === true);
  }

  // ----------------------------------------------------------------------------------------------
  get hasRightBranch(): boolean {
    return (this._branch && this._hasLeftBranch === false);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private bringThisToTop(): void {
    const group = <Phaser.Group> this.parent;
    if (group) {
      group.bringToTop(this);
    }
  }

  // ----------------------------------------------------------------------------------------------
  private chopBranch(isPlayerOnLeftSide: boolean): void {
    if (!this._branch) {
      return;
    }

    const branch = this._branch;
    const xLog = branch.x;
    const yLog = branch.y;
    branch.anchor.set(0.5);
    branch.left = xLog;
    branch.top = yLog;

    const xTarget = isPlayerOnLeftSide
      ? this.game.scale.width + -this.x + branch.width
      : -(this.x + branch.width);

    const tweens = this.game.tweens;
    tweens.create(branch).to(
      {
        x: xTarget,
      },
      TreeLog.CHOP_DROP_DURATION_MS,
      Phaser.Easing.Linear.None,
    ).start();

    const fallingTween = tweens.create(branch).to(
      {
        y: this.game.scale.height + branch.height,
      },
      TreeLog.CHOP_DROP_DURATION_MS,
      Phaser.Easing.Back.In,
    ).start();

    const spinningTween = tweens.create(branch).to(
      {
        rotation: isPlayerOnLeftSide ? Math.PI : -Math.PI,
      },
      TreeLog.CHOP_SPIN_DURATION_MS,
      Phaser.Easing.Linear.None,
      true,
      0,
      -1,
    ).start();

    fallingTween.onComplete.addOnce(() => {
      spinningTween.stop();
    });
  }

  // ----------------------------------------------------------------------------------------------
  private chopLog(isPlayerOnLeftSide: boolean): Promise<this> {
    return (new Promise((resolve) => {
      const log = this._log;
      const xLog = log.x;
      const yLog = log.y;
      log.anchor.set(0.5);
      log.left = xLog;
      log.top = yLog;

      const xTarget = isPlayerOnLeftSide
        ? this.game.scale.width + -this.x + log.width
        : -(this.x + log.width);

      const tweens = this.game.tweens;
      tweens.create(log).to(
        {
          x: xTarget,
        },
        TreeLog.CHOP_DROP_DURATION_MS,
        Phaser.Easing.Linear.None,
      ).start();

      const fallingTween = tweens.create(log).to(
        {
          y: this.game.scale.height + log.height,
        },
        TreeLog.CHOP_DROP_DURATION_MS,
        Phaser.Easing.Cubic.In,
      ).start();

      const spinningTween = tweens.create(log).to(
        {
          rotation: isPlayerOnLeftSide ? Math.PI : -Math.PI,
        },
        TreeLog.CHOP_SPIN_DURATION_MS,
        Phaser.Easing.Linear.None,
        true,
        0,
        -1,
      ).start();

      fallingTween.onComplete.addOnce(() => {
        spinningTween.stop();
        resolve(this);
      });
    }));
  }

  // ----------------------------------------------------------------------------------------------
  private initBranch(createBranchChance: number): void {
    if (createBranchChance <= Math.random()) {
      this._hasLeftBranch = undefined;
      return;
    }

    this._hasLeftBranch = Math.random() >= 0.5;
    if (this._hasLeftBranch) {
      this.initLeftBranch();
    } else {
      this.initRightBranch();
    }
  }

  // ----------------------------------------------------------------------------------------------
  private initLeftBranch(): void {
    this._branch = this.game.add.image(
      TreeLog.LEFT_BRANCH_X,
      TreeLog.BRANCH_Y,
      'tree-left-branch',
      undefined,
      this,
    );
  }

  // ----------------------------------------------------------------------------------------------
  private initLog(): void {
    this._log = this.game.add.image(0, 0, 'tree-log', undefined, this);
  }

  // ----------------------------------------------------------------------------------------------
  private initRightBranch(): void {
    this._branch = this.game.add.image(
      TreeLog.RIGHT_BRANCH_X,
      TreeLog.BRANCH_Y,
      'tree-right-branch',
      undefined,
      this,
    );
  }
}
