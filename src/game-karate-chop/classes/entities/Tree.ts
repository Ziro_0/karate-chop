import TreeLog from './TreeLog';

export default class Tree extends Phaser.Group {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static BASE_X_POSITION = 191;

  private static TRUNK_Y_POSITION = 477;

  private static LOG_DROP_DURATION_MS = 80;

  private _logs: TreeLog[];

  private _stump: Phaser.Image;

  private _viewOffset: number;

  private _createBranchChance: number;

  private _areLogsDropping: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, viewOffset: number, parent?: PIXI.DisplayObjectContainer) {
    super(game, parent);
    this._viewOffset = viewOffset;
    this.initStump();
    this.initLogs();
  }

  // ----------------------------------------------------------------------------------------------
  get areLogsDropping(): boolean {
    return (this._areLogsDropping);
  }

  // ----------------------------------------------------------------------------------------------
  isNextBranchOnLeftSide(): boolean {
    return (this._logs[1].hasLeftBranch);
  }

  // ----------------------------------------------------------------------------------------------
  isNextBranchOnRightSide(): boolean {
    return (this._logs[1].hasRightBranch);
  }

  // ----------------------------------------------------------------------------------------------
  set createBranchChance(value: number) {
    this._createBranchChance = value;
  }

  // ----------------------------------------------------------------------------------------------
  timberrrrr(isPlayerOnLeftSide: boolean): boolean {
    if (this.areLogsDropping) {
      return (false);
    }

    if (!this.chopBottomLog(isPlayerOnLeftSide)) {
      return (false);
    }

    this.initReplacementLog();
    this.dropLogs();
    return (true);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private chopBottomLog(isPlayerOnLeftSide: boolean): boolean {
    const log = this._logs.shift();
    if (!log) {
      // sanity check
      return (false);
    }

    log.chop(isPlayerOnLeftSide)
      .then((log) => {
        this.removeLog(log);
      });

    return (true);
  }

  // ----------------------------------------------------------------------------------------------
  private dropLogs(): void {
    this._areLogsDropping = true;

    for (let index = this._logs.length - 1; index >= 0; index -= 1) {
      const log = this._logs[index];
      let yTarget: number;
      if (index === 0) {
        yTarget = this._stump.y - log.height;
      } else {
        yTarget = this._logs[index - 1].y;
      }

      const tween = this.game.tweens.create(log).to(
        {
          y: yTarget,
        },
        Tree.LOG_DROP_DURATION_MS,
        Phaser.Easing.Cubic.In,
      ).start();

      if (index === 0) {
        tween.onComplete.addOnce(() => {
          this._areLogsDropping = false;
        });
      }
    }
  }

  // ----------------------------------------------------------------------------------------------
  private initLog(y: number): TreeLog {
    const x = Tree.BASE_X_POSITION;
    const log = new TreeLog(this.game, x, y, this._createBranchChance, this);
    y -= log.height;
    log.y = y;
    this._logs.push(log);
    return (log);
  }

  // ----------------------------------------------------------------------------------------------
  private initLogs(): void {
    this._logs = [];
    this._createBranchChance = 0;

    let y = this._stump.y;
    let done = false;
    do {
      const log = this.initLog(y);
      y = log.y;
      done = log.bottom < 0;
    } while (!done);

    // add one more to keep logs visibility seamless at the top when they are moving
    this.initLog(y);
  }

  // ----------------------------------------------------------------------------------------------
  private initReplacementLog(): void {
    const topLog = this._logs[this._logs.length - 1];
    const y = topLog.y;
    this.initLog(y);
  }

  // ----------------------------------------------------------------------------------------------
  private initStump(): void {
    const x = Tree.BASE_X_POSITION;
    const y = Tree.TRUNK_Y_POSITION;
    this._stump = this.game.add.image(x, y + this._viewOffset, 'tree-stump', undefined, this);
  }

  // ----------------------------------------------------------------------------------------------
  private removeLog(log: TreeLog): void {
    log.destroy();
  }
}
