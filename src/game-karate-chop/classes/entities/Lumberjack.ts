import LumberjackAnimations from '../animations/LumberjackAnimations';

export default class Lumberjack extends Phaser.Sprite {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  private static readonly LEFT_POSITION = 80;

  private static readonly RIGHT_POSITION = 290;

  private static readonly Y_POSITION = 384;

  private static readonly DEFEATED_FLASH_RATE_MS = 80;

  sprite: Phaser.Sprite;

  private _defeatedFlashTimer: Phaser.Timer;

  private _isOnLeftSide: boolean;

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  constructor(game: Phaser.Game, viewOffset: number) {
    super(game, 0, Lumberjack.Y_POSITION + viewOffset, 'lumberjack1_7x2', 0);
    this.anchor.x = 0.5;
    this.isOnLeftSide = true;
    this.initAnimations();
  }

  // ----------------------------------------------------------------------------------------------
  attack(): void {
    const animKey = Phaser.ArrayUtils.getRandomItem([
      LumberjackAnimations.ANIM_KICK,
      LumberjackAnimations.ANIM_PUNCH,
    ]);

    this.animations.stop();
    this.play(animKey, undefined, false);
  }

  // ----------------------------------------------------------------------------------------------
  defeat(): void {
    this.startFlash();
  }

  // ----------------------------------------------------------------------------------------------
  destroy(destroyChildren?: boolean): void {
    this.stopFlash();
    super.destroy(destroyChildren);
  }

  // ----------------------------------------------------------------------------------------------
  idle(): void {
    this.play(LumberjackAnimations.ANIM_IDLE, undefined, true);
  }

  // ----------------------------------------------------------------------------------------------
  get isOnLeftSide(): boolean {
    return (this._isOnLeftSide);
  }

  // ----------------------------------------------------------------------------------------------
  set isOnLeftSide(value: boolean) {
    if (this._isOnLeftSide === value) {
      return;
    }

    this._isOnLeftSide = value;

    if (value) {
      this.scale.x = 1;
      this.left = Lumberjack.LEFT_POSITION;
    } else {
      this.left = Lumberjack.RIGHT_POSITION;
      this.scale.x = -1;
    }
  }

  // ----------------------------------------------------------------------------------------------
  revyve(): void {
    this.stopFlash();
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private initAnimations(): void {
    LumberjackAnimations.Create(this);

    this.events.onAnimationComplete.add((_object: Lumberjack, anim: Phaser.Animation) => {
      if (anim.name === LumberjackAnimations.ANIM_KICK
        || anim.name ===LumberjackAnimations.ANIM_PUNCH) {
        this.idle();
      }
    });
  }

  // ----------------------------------------------------------------------------------------------
  private startFlash(): void {
    if (this._defeatedFlashTimer) {
      return;
    }

    this._defeatedFlashTimer = this.game.time.create();

    this._defeatedFlashTimer.loop(Lumberjack.DEFEATED_FLASH_RATE_MS, () => {
      this.visible = !this.visible;
    });

    this._defeatedFlashTimer.start();
  }

  // ----------------------------------------------------------------------------------------------
  private stopFlash(): void {
    if (this._defeatedFlashTimer) {
      this._defeatedFlashTimer.destroy();
      this._defeatedFlashTimer = null;
    }

    this.visible = true;
  }
}