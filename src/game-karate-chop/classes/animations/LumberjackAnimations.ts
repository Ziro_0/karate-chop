interface ILumberjackAnimation {
  name: string;
  frames: number[];
  fps: number;
}

export default class LumberjackAnimations {
  // ==============================================================================================
  // properties
  // ==============================================================================================
  static readonly ANIM_IDLE = 'idle';
  static readonly ANIM_PUNCH = 'punch';
  static readonly ANIM_KICK = 'kick';

  // ==============================================================================================
  // public
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  public static Create(lumberjackSprite: Phaser.Sprite): boolean {
    if (!lumberjackSprite) {
      return (false);
    }

    const data = lumberjackSprite.game.cache.getJSON('lumberjack1_7x2_animations');
    if (!data) {
      return (false);
    }

    const animationsData = data.animations;
    if (!animationsData) {
      return (false);
    }

    const idle = LumberjackAnimations.BuiltAnimationData(animationsData,
      LumberjackAnimations.ANIM_IDLE);
    if (!idle) {
      return (false);
    }

    const playerPunch = LumberjackAnimations.BuiltAnimationData(animationsData,
      LumberjackAnimations.ANIM_PUNCH);
    if (!playerPunch) {
      return (false);
    }

    const playerKick = LumberjackAnimations.BuiltAnimationData(animationsData,
      LumberjackAnimations.ANIM_KICK);
    if (!playerKick) {
      return (false);
    }

    lumberjackSprite.animations.add(idle.name, idle.frames, idle.fps);
    lumberjackSprite.animations.add(playerPunch.name, playerPunch.frames, playerPunch.fps);
    lumberjackSprite.animations.add(playerKick.name, playerKick.frames, playerKick.fps);

    return (true);
  }

  // ==============================================================================================
  // private
  // ==============================================================================================

  // ----------------------------------------------------------------------------------------------
  private static BuiltAnimationData(animationsData: any, key: string): ILumberjackAnimation {
    const animiationData = animationsData[key];
    if (!animiationData) {
      return (null);
    }

    const framesData: string = animiationData.frames;
    if (typeof (framesData) !== 'string') {
      return (null);
    }

    const fpsData: number = animiationData.fps;
    if (typeof (fpsData) !== 'number') {
      return (null);
    }

    return ({
      name: key,
      frames: framesData.split(',').map((frame) => Number.parseInt(frame)),
      fps: fpsData,
    });
  }
}
