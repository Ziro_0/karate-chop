// ================================================================================================
export interface ILevelData {
  /**
   * number of logs the player must chop before winning this level. Required.
   */
  numberOfChopsToWin: number;

  /**
   * Determines the chance from `0.0` (0%) to `1.0` (100%) that each new log will also
   * generate a branch. Required.
   */
  createBranchChance: number;

  /**
   * Points awarded for chopping a log on this level.
   * 
   * Optional, and defaults to `1`.
   */
  pointsPerChop?: number;

  /**
   * Number of points lost each time player is hit by a log while on this level.
   * 
   * Optional, and defaults to `1`.
   */
  pointsPenalty?: number;

  /**
   * Penalty enforced on player if are hit by a log. Note that is not related to the
   * `cantMoveDurationInSeconds` option in `IKarateChopGameConfig`.
   * 
   * Possible values are:
   * 
   * `restart-level` - Player's number of chops resets to 0 for this level.
   * 
   * `reduce-level` - Same as `restart`, and player's level is reduced by one
   * 
   * `start-over` - Same as `restart`, and player's level starts over!
   * 
   * `none` - No penalty is enforced.
   * 
   * Optional, and defaults to `none`.
   */
  levelPenalty?: 'restart-level' | 'reduce-level' | 'start-over' | 'none';

  /**
   * Time bonus (if any), in seconds, awarded to player for completing the level.
   * 
   * Optional, and defaults to `10`.
   */
  timeBonus?: number;

  /**
   * Sets the color (tint) of the progress bar fill. Specify a color in RRGGBB format.
   * 
   * Optional, and defaults to `0xff0000` (red)
   */
  progressBarColor?: number;
}

// ================================================================================================
/**
 * Specifies game config properties for "karate-chop" game.
 */
export default interface IKarateChopGameConfig {
  /**
   * Game start time, in seconds. Required.
   */
  startTimeInSeconds: number;

  /**
   * Array of level data describing each level. Required.
   * 
   * Each time the player completes a level, they will proceed to the next one. If the player wins
   * the last level, the game stays on that level (unless a level penalty says otherwise).
   * 
   * You can define as many levels as you want, but there must be at least one level defined.
   * 
   * See `ILevelData` for more details.
   */
  levels: ILevelData[];

  /**
   * Should the screen shake and flash when a log is chopped or player is hit by a branch?
   * 
   * Optional, and defaults to `true`.
   */
  isUiImmersive?: boolean;

  /**
   * Should the game show the help at the start?
   * 
   * Optional, and defaults to `true`.
   */
  shouldShowHelp?: boolean;
  
  /**
   * number of seconds the player is frozen if the player is hit by a branch.
   * 
   * Optional, and defaults to `1.5`.
   */
  cantMoveDurationInSeconds?: number;

  /**
   * Sets the level on which the player starts. Useful for debugging later levels.
   * 
   * Optional, and defaults to `0`.
   */
  startLevel?: number;
}
