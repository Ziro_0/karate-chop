@echo off

set SOURCE_BUILD_DIR=.\www
set TARGET_BUILD_DIR=C:\Apache24\htdocs\projects\hardeep\karate-chop

if exist %TARGET_BUILD_DIR% (
  echo removing target build directory "%TARGET_BUILD_DIR%"...
  rd /s /q "%TARGET_BUILD_DIR%"
)

echo copying folder %SOURCE_BUILD_DIR% to %TARGET_BUILD_DIR%...
xcopy /e /i /y "%SOURCE_BUILD_DIR%" "%TARGET_BUILD_DIR%"

if %ERRORLEVEL% neq 0 goto ErrorHappened

echo Boom. Done.
pause
goto:eof

:ErrorHappened
echo An error occurred...
pause